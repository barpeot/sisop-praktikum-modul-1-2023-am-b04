# Sisop-Praktikum-Modul-1-2023-AM-B04

## Anggota Kelompok B04:
+ Akbar Putra Asenti Priyanto (5025211004)
+ Muhammad Rafi Sutrisno (5025211167)
+ Muhammad Rafi Insan Fillah (5025211169)

# Soal 1

## Penjelasan Script
1.a. Menampilkan 5 Universitas dengan ranking tertinggi di Jepang. Hasil tersebut dapat diperoleh dengan 2 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk delimiter simbol ',' pada kolom ketiga yang bernilai "JP" dari file csv

	> Hasil filter tersebut dijadikan input untuk mengambil hanya 5 baris teratas menggunakan head command

1.b. Mencari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas dari hasil filter poin a. Hal tersebut dapat diperoleh dengan 3 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk sama seperti poin 1.1.

	> Hasil filter tersebut dijadikan input untuk mengurutkan baris berdasarkan fsr score menggunakan command sort options baris ke-9

	> Hasil sorting kemudian dijadikan input untuk mengambil hanya 5 baris teratas menggunakan head command

1.c. Menampilkan 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi. Hal tersebut dapat diperoleh mirip dengan poin 1.b. dengan 3 tahap:

	> Filter universitas dengan location code "JP" menggunakan awk sama seperti poin 1.b.

	> Hasil filter tersebut dijadikan input untuk mengurutkan baris berdasarkan ger rank menggunakan command sort options baris ke-20
    
	> Hasil sorting kemudian dijadikan input untuk mengambil hanya 10 baris teratas menggunakan head command

1.d. Mencari universitas tersebut dengan kata kunci "keren". Karena tidak dijelaskan secara eksplisit kata kunci tersebut ada pada kolom berapa maka dilakukan search pattern pada seluruh kolom pada csv file. Filter tersebut menggunakan awk dengan pattern yg dicari '/keren/' dengan input file csv.

## university_survey.sh

```
#!/bin/bash

#poin 1
echo "poin 1:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'|head -n 5

#poin 2
echo "poin 2:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t',' -k 9,9 -n |head -n 5 

#poin 3
echo "poin 3"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t ',' -k 20,20 -n| head -n 10

#poin 4
echo "poin 4:"
awk '/Keren/ {print}' '2023 QS World University Rankings.csv'

```

# Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 
    
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
        
+ File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
        
+ File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
    
+ Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). 

## kobeni_liburan.sh

```
#!/bin/bash
#mendaftarkan cron bila belum terdaftar
spath=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )"/kobeni_liburan.sh"
crontab -l | grep -q 'kobeni_liburan.sh' && stat=1 || stat=0

if [ $stat == 0 ]; then
crontab -l > mycron
echo "0 * * * * bash $spath" >> mycron
crontab mycron
rm mycron
fi

path=$( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd )
if [[ $(($(date +'%s / 3600 % 10 '))) -eq 0 ]]; then
 #Membuat folder dan mendownload file
 prefix='kumpulan_'
 n=1

 #Cari folder kumpulan_ yang paling baru
 while [ -d $path/$prefix$n ]; do ((n++))
 done

 #Buat folder
 echo "Membuat $prefix$n..."
 mkdir $path/$prefix$n
 echo "$prefix$n telah dibuat"

 #Cari tahu jam, jika 0 maka diloop sekali
 times=$(date +%H)
 if [ $times == 00 ]; then
 times=1
 echo "$times"
 fi

 #Download file berdasarkan jam
 for((i=1;i<=times;i=i+1))
 do
  echo "Sedang mengambil perjalanan_$i.png..."
  wget -O $path/$prefix$n/perjalanan_$i.png https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Indonesia_map_with_name_of_islands.png/800px-Indonesia_map_with_name_of_islands.png
  echo "Selesai mengambil perjalanan_$i.png"
 done
fi

#Jika jam 00:00 maka akan melakukan zip
if [[ $(($(date +'%s / 3600 % 24 '))) -eq 17 ]]; then
 hour=$(date +%H)
 if [ $hour == 00 ]; then
  devil='devil_'
  d=1

  while [ `ls $path | grep $devil$d.zip` ]; do ((d++))
  done

  cd $path
  zip $devil$d -rm `find . -name "kumpulan_*" -print`
 fi
fi
```

## Penjelasan Script

# Soal 3

## Penjelasan Script

# Soal 4

## Penjelasan Script
