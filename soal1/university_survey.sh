#!/bin/bash

#poin 1
echo "poin 1:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'|head -n 5

#poin 2
echo "poin 2:"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t',' -k 9,9 -n |head -n 5 

#poin 3
echo "poin 3"
awk -F',' '$3=="JP" {print}' '2023 QS World University Rankings.csv'| sort -t ',' -k 20,20 -n| head -n 10

#poin 4
echo "poin 4:"
awk '/Keren/ {print}' '2023 QS World University Rankings.csv'
